import { Module } from '@nestjs/common'
import { HiveSqlService } from './hiveSql.service'

@Module({
  imports: [],
  controllers: [],
  providers: [HiveSqlService],
  exports: [HiveSqlService]
})
export class HiveSqlModule {}
