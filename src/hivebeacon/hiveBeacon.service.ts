import { Injectable, ForbiddenException } from '@nestjs/common'
import { HttpService } from '@nestjs/axios'
import { map, lastValueFrom, catchError } from 'rxjs'

@Injectable()
export class HiveBeaconService {
  constructor(private readonly httpService: HttpService) {}
  
  async getRcCosts() { //: Observable<AxiosResponse<any[]>> {
    const request = this.httpService.get('https://beacon.peakd.com/api/rc/costs')
      .pipe(
        map(response => response.data?.costs)
      )
      .pipe(
        catchError(() => {
          throw new ForbiddenException('Failed getting data from Hive Beacon API')
        }),
      )
    
    const costs = await lastValueFrom(request)
    return costs
  }
  
}
