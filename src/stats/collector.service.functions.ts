import { Injectable, Logger } from '@nestjs/common'
import { InjectConnection } from '@nestjs/sequelize'
import { QueryTypes, Sequelize } from 'sequelize'
import { HiveSqlService } from '../hivesql/hiveSql.service'
import { LocalConnectionService } from '../localconnection/localConnection.service'
import { HiveBeaconService } from '../hivebeacon/hiveBeacon.service'
import { SettingsService } from '../settings/settings.service'

@Injectable()
export class CollectorServiceFunctions {
  private readonly logger = new Logger(CollectorServiceFunctions.name)
  private settings = new SettingsService
  private exchanges = this.settings.exchanges

  constructor(
    @InjectConnection('localConnection')
    private sequelize: Sequelize,
    private hiveSqlService: HiveSqlService,
    private localConnectionService: LocalConnectionService,
    private hiveBeaconService: HiveBeaconService,
  ) {}
  
  /*
   * Get the string of a date
   */
  async getDateString(date: Date) {
    let dd = String(date.getUTCDate()).padStart(2, '0')
    let mm = String(date.getUTCMonth() + 1).padStart(2, '0')
    let yyyy = date.getUTCFullYear()
    return yyyy + '-' + mm + '-' + dd + " 00:00:00"
  }

  /*
   * Collect daily post stats
   */
  async collectDailyPostStats(today: string) {
    
    //Load the daily stats we already have
    const [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_comments_daily
      WHERE type = 'posts'
      ORDER BY day DESC
      LIMIT 1;`,
      []
    )
    
    //No stats yet in our database - populate from scratch
    if(localStats.length == 0) {
      const posts = await this.hiveSqlService.executeQuery(
      `SELECT YEAR(created) AS year, MONTH(created) AS month, DAY(created) AS day, COUNT(*) AS count
      FROM comments
      WHERE depth = 0
      AND created < ?
      GROUP BY YEAR(created), MONTH(created), DAY(created)
      ORDER BY year, month, day ASC;`,
      [today]
      )
      this.logger.log(`Fetched ${posts.length} records`)
      
      posts.forEach(async (post) => {
        await this.localConnectionService.executeQuery(
        `INSERT INTO hive_comments_daily (day, type, total_num)
        VALUES (make_date(?, ?, ?), 'posts', ?);`,
        [post.year, post.month, post.day, post.count])
      })
    }
    //We have stats in our database
    else {
      //Get only post stats after last fetched date and before today
      let lastDate = localStats[0].day + " 23:59:59"
      
      const posts = await this.hiveSqlService.executeQuery(
      `SELECT YEAR(created) AS year, MONTH(created) AS month, DAY(created) AS day, COUNT(*) AS count
      FROM comments
      WHERE depth = 0
      AND created > ?
      AND created < ?
      GROUP BY YEAR(created), MONTH(created), DAY(created)
      ORDER BY year, month, day ASC;`,
      [lastDate, today]
      )
      this.logger.log(`Fetched ${posts.length} records`)
      
      //Add new post stats for missing days, if any
      posts.forEach(async (post) => {
        await this.localConnectionService.executeQuery(
        `INSERT INTO hive_comments_daily (day, type, total_num)
        VALUES (make_date(?, ?, ?), 'posts', ?);`,
        [post.year, post.month, post.day, post.count])
      })
    }
    
    return
  }
  
  
  /*
   * Collect daily comment stats
   */
  async collectDailyCommentStats(today: string) {
    
    //Load the daily stats we already have
    const [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_comments_daily
      WHERE type = 'comments'
      ORDER BY day DESC
      LIMIT 1;`,
      []
    )

    //No stats yet in our database - populate from scratch
    if(localStats.length == 0) {
      const comments = await this.hiveSqlService.executeQuery(
      `SELECT YEAR(created) AS year, MONTH(created) AS month, DAY(created) AS day, COUNT(*) AS count
      FROM comments
      WHERE depth <> 0
      AND created < ?
      GROUP BY YEAR(created), MONTH(created), DAY(created)
      ORDER BY year, month, day ASC;`,
      [today]
      )
      this.logger.log(`Fetched ${comments.length} records`)
      
      comments.forEach(async (comment) => {
        await this.localConnectionService.executeQuery(
        `INSERT INTO hive_comments_daily (day, type, total_num)
        VALUES (make_date(?, ?, ?), 'comments', ?);`,
        [comment.year, comment.month, comment.day, comment.count])
      })
    }
    //We have stats in our database
    else {
      //Get only comment stats after last fetched date and before today
      let lastDate = localStats[0].day + " 23:59:59"
      
      const comments = await this.hiveSqlService.executeQuery(
      `SELECT YEAR(created) AS year, MONTH(created) AS month, DAY(created) AS day, COUNT(*) AS count
      FROM comments
      WHERE depth <> 0
      AND created > ?
      AND created < ?
      GROUP BY YEAR(created), MONTH(created), DAY(created)
      ORDER BY year, month, day ASC;`,
      [lastDate, today]
      )
      this.logger.log(`Fetched ${comments.length} records`)
      
      //Add new comment stats for missing days, if any
      comments.forEach(async (comment) => {
        await this.localConnectionService.executeQuery(
        `INSERT INTO hive_comments_daily (day, type, total_num)
        VALUES (make_date(?, ?, ?), 'comments', ?);`,
        [comment.year, comment.month, comment.day, comment.count])
      })
    }
    
    return
  }
  
  /*
   * Collect daily operations stats
   */
  async collectDailyOperationStats(today: string) {

    //all operations
    for(const operation of this.settings.operationTypes) {
      try {
        await this.collectDailyOperationMetrics(today, operation)
      }
      catch (e) {
        this.logger.error(`Error collecting ${operation} operation metrics`, e)
      }
    }
    
    //custom_json stats
    for(var app of this.settings.trackedApps) {
      for(var appName in app) {
        try {
          await this.collectDailyCustomJsonMetrics(today, appName, app[appName])
        }
        catch (e) {
          this.logger.error(`Error collecting ${appName} custom_jsons`, e)
        }
      }
    }
  }
  
  /*
   * Collect daily Resource Credit stats
   */
  async collectDailyRcStats(today: string) {
    
    //Turn timestamp to just date
    today = today.substr(0, 10)
    
    //Check if we have collected RC stats today
    const [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_rc_daily
      WHERE metric_name = 'rc_cost_vote_operation'
      AND day = ?;`,
      [today]
    )

    //If not, collect RC stats for the day
    if(localStats.length == 0) {
      const rcStats = await this.hiveBeaconService.getRcCosts()
      
      const tr = await this.sequelize.transaction();
      
      try {
        for (const rcStat of rcStats) {
          let rcMetricName = 'rc_cost_' + rcStat.operation
          await this.localConnectionService.executeTranQuery(
          `INSERT INTO hive_rc_daily (day, metric_name, value)
          VALUES (?, ?, ?);`,
          [today, rcMetricName, rcStat.rc_needed],
          tr)

          let hpMetricName = 'hp_equiv_' + rcStat.operation
          await this.localConnectionService.executeTranQuery(
          `INSERT INTO hive_rc_daily (day, metric_name, value)
          VALUES (?, ?, ?);`,
          [today, hpMetricName, (rcStat.hp_needed * 10000)],
          tr)
        }
        await tr.commit()
      } catch (e) {
        this.logger.error('Error:', e)
        await tr.rollback()
      }
    }
    
    return
  }

  /*
   * Collect daily market stats
   */
  async collectDailyMarketStats(today: string) {
    
    try {
      await this.collectDailyTotalSupplyMetrics(today)
    }
    catch (e) {
      this.logger.error('Error collecting total_supply_metrics_hive', e)
    }

  }

  /*
   * Collect daily operations specific metrics
   */
  async collectDailyOperationMetrics(today: string, operation: string) {

    //Load the daily stats we already have
    const [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_operations_daily
      WHERE metric_name = ?
      ORDER BY day DESC
      LIMIT 1;`,
      [operation]
    )
    
    //Collect since last collected date, otherwise since beginning of blockchain 
    let lastDate = (localStats.length > 0) ? localStats[0].day + " 23:59:59" : "1970-01-01 00:00:00"

    //Map operations to HiveSQL tables
    let table = ''
    if(operation == 'claim_account') table = 'TxAccountClaims'
    if(operation == 'change_recovery_account') table = 'TxAccountRecoveryChanges'
    if(operation == 'request_account_recovery') table = 'TxAccountRecoveryRequests'
    if(operation == 'recover_account') table = 'TxAccountRecoveryConfirms'
    if(operation == 'account_update') table = 'TxAccountUpdates'
    if(operation == 'account_update2') table = 'TxAccountUpdates2'
    if(operation == 'account_witness_proxy') table = 'TxAccountWitnessProxies'
    if(operation == 'account_witness_vote') table = 'TxAccountWitnessVotes'
    if(operation == 'claim_reward_balance') table = 'TxClaimRewardBalances'
    if(operation == 'collateralized_convert') table = 'TxCollateralizedConverts'
    if(operation == 'comment') table = 'TxComments'
    if(operation == 'comment_options') table = 'TxCommentsOptions'
    if(operation == 'convert') table = 'TxConverts'
    if(operation == 'custom_json') table = 'TxCustoms'
    if(operation == 'decline_voting_rights') table = 'TxDeclineVotingRights'
    if(operation == 'delegate_vesting_shares') table = 'TxDelegateVestingShares'
    if(operation == 'delete_comment') table = 'TxDeleteComments'
    if(operation == 'escrow_approve') table = 'TxEscrowApproves'
    if(operation == 'escrow_dispute') table = 'TxEscrowDisputes'
    if(operation == 'escrow_release') table = 'TxEscrowReleases'
    if(operation == 'escrow_transfer') table = 'TxEscrowTransfers'
    if(operation == 'feed_publish') table = 'TxFeeds'
    if(operation == 'limit_order_cancel') table = 'TxLimitOrdersCancels'
    if(operation == 'pow') table = 'TxPows'
    if(operation == 'pow2') return //The previous one covers this one. Bundled both into one since they're not differentiated in HiveSQL.
    if(operation == 'create_proposal') table = 'TxProposalCreates'
    if(operation == 'remove_proposal') table = 'TxProposalRemoves'
    if(operation == 'update_proposal') table = 'TxProposalUpdates'
    if(operation == 'update_proposal_votes') table = 'TxProposalVoteUpdates'
    if(operation == 'recurrent_transfer') table = 'TxRecurrentTransfers'
    if(operation == 'vote') table = 'TxVotes'
    if(operation == 'withdraw_vesting') table = 'TxWithdraws'
    if(operation == 'set_withdraw_vesting_route') table = 'TxWithdrawVestingRoutes'
    if(operation == 'witness_set_properties') table = 'TxWitnessSetProperties'
    if(operation == 'witness_update') table = 'TxWitnessUpdates'
    
    let sql = ''
    switch(operation) {
      case 'account_create':
        sql = `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, COUNT(*) AS num_operations
        FROM TxAccountCreates
        WHERE delegation = 0
        AND fee <> 0`
        break
      case 'account_create_with_delegation':
        sql = `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, COUNT(*) AS num_operations
        FROM TxAccountCreates
        WHERE delegation <> 0`
        break
      case 'create_claimed_account':
        sql = `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, COUNT(*) AS num_operations
        FROM TxAccountCreates
        WHERE delegation = 0
        AND fee = 0`
        break
      case 'limit_order_create':
        sql = `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, COUNT(*) AS num_operations
        FROM TxLimitOrdersCreates
        WHERE exchange_rate IS NULL`
        break
      case 'limit_order_create2':
        sql = `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, COUNT(*) AS num_operations
        FROM TxLimitOrdersCreates
        WHERE exchange_rate IS NOT NULL`
        break
      case 'transfer':
      case 'transfer_to_vesting':
      case 'transfer_to_savings':
      case 'transfer_from_savings':
      case 'cancel_transfer_from_savings':
        sql = `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, COUNT(*) AS num_operations
        FROM TxTransfers
        WHERE type = ${operation}`
        break
      default:
        sql = `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, COUNT(*) AS num_operations
        FROM ${table}
        WHERE 1=1`
        break
    }
    
    const ops = await this.hiveSqlService.executeQuery(
    sql + ` 
    AND timestamp > :lastDateStr
    AND timestamp < :todayStr
    GROUP BY YEAR(timestamp), MONTH(timestamp), DAY(timestamp)
    ORDER BY year, month, day ASC;`,
    {lastDateStr: lastDate, todayStr: today}
    )
    this.logger.log(`Fetched ${ops.length} records`)

    const tr = await this.sequelize.transaction();
    
    try {
      for (const op of ops) {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_operations_daily (day, metric_name, value)
        VALUES (make_date(?, ?, ?), ?, ?);`,
        [op.year, op.month, op.day, operation, op.num_operations],
        tr)
      }
      await tr.commit()
    } catch (e) {
      this.logger.error('Error:', e)
      await tr.rollback()
    }
    
    return
  }

  /*
   * Collect daily custom_json metrics
   */
  async collectDailyCustomJsonMetrics(today: string, appName: string, assocCustomJsons: string) {
    
    
    //Load the daily stats we already have
    if(appName !== "") {
      var [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
        `SELECT day
        FROM hive_operations_daily
        WHERE app = ?
        ORDER BY day DESC
        LIMIT 1;`,
        [appName]
      )
    }
    //else { //Hive core custom_jsons
    //  var [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
    //    `SELECT day
    //    FROM hive_operations_daily
    //    WHERE metric_name = ?
    //    ORDER BY day DESC
    //    LIMIT 1;`,
    //    [this.settings[assocCustomJsons][0]]
    //  )
    //}
    
    //Collect since last collected date, otherwise since beginning of blockchain 
    let lastDate = (localStats.length > 0) ? localStats[0].day + " 23:59:59" : "1970-01-01 00:00:00"
    
    //Max of 30 custom_jsons, to keep response time workable
    let customJsons = JSON.parse(JSON.stringify(this.settings[assocCustomJsons])) //This makes a copy so that the original will not be affected when we splice later
    let iterations = Math.ceil(customJsons.length / 30)
    let operationsAll = []
    
    for(var i = 1; i <= iterations; i++) {
      let customJsonsBatch = customJsons.splice(0, 30)
      
      var operations = await this.hiveSqlService.executeQuery(
      `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, tid, COUNT(*) AS count
      FROM TxCustoms
      WHERE tid IN(:customJsonIds)
      AND timestamp > :lastDateStr
      AND timestamp < :todayStr
      GROUP BY YEAR(timestamp), MONTH(timestamp), DAY(timestamp), tid
      ORDER BY year, month, day ASC;`,
      {customJsonIds: customJsonsBatch, lastDateStr: lastDate, todayStr: today}
      )
      this.logger.log(`Fetched ${operations.length} records`)

      operationsAll = operationsAll.concat(operations)
    }

    const tr = await this.sequelize.transaction();
    
    try {
      for (const operation of operationsAll) {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_operations_daily (day, metric_name, app, value)
        VALUES (make_date(?, ?, ?), ?, ?, ?);`,
        [operation.year, operation.month, operation.day, operation.tid, appName, operation.count],
        tr)
      }
      await tr.commit()
    } catch (e) {
      this.logger.error('Error:', e)
      await tr.rollback()
    }
    
    return
  }

  /*
   * Collect daily total supply metrics
   */
  async collectDailyTotalSupplyMetrics(today: string) {
    
    //Check if we have collected supply stats today
    const [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_market_daily
      WHERE metric_name = ?
      AND day = ?;`,
      ['total_hive_supply', today]
    )

    //If not, collect total supply stats for the day
    if(localStats.length == 0) {
      
      let supply = await this.hiveSqlService.executeQuery(
      `SELECT virtual_supply, current_supply, current_hbd_supply
      FROM DynamicGlobalProperties;`,
      {}
      )
      this.logger.log(`Fetched ${supply.length} records`)
      
      const tr = await this.sequelize.transaction();

      try {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_market_daily (day, metric_name, value)
        VALUES (?, ?, ?);`,
        [today, 'total_virtual_supply', supply[0].virtual_supply],
        tr)

        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_market_daily (day, metric_name, value)
        VALUES (?, ?, ?);`,
        [today, 'total_hive_supply', supply[0].current_supply],
        tr)

        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_market_daily (day, metric_name, value)
        VALUES (?, ?, ?);`,
        [today, 'total_hbd_supply', supply[0].current_hbd_supply],
        tr)
        await tr.commit()
      } catch (e) {
        this.logger.error('Error:', e)
        await tr.rollback()
      }
    }
    
    return
  }

}
