import { Injectable, Logger } from '@nestjs/common'
import { InjectConnection } from '@nestjs/sequelize'
import { QueryTypes, Sequelize } from 'sequelize'
import { HiveSqlService } from '../hivesql/hiveSql.service'
import { LocalConnectionService } from '../localconnection/localConnection.service'
import { SettingsService } from '../settings/settings.service'
import { CollectorServiceFunctions } from './collector.service.functions'

@Injectable()
export class CollectorServiceCommunities {
  private readonly logger = new Logger(CollectorServiceCommunities.name)
  private settings = new SettingsService

  constructor(
    @InjectConnection('localConnection')
    private sequelize: Sequelize,
    private hiveSqlService: HiveSqlService,
    private localConnectionService: LocalConnectionService,
    private collectorServiceFunctions: CollectorServiceFunctions
  ) {}
  
  /*
   * Collect daily communities stats
   */
  async collectDailyCommunitiesStats(today: string) {

    try {
      await this.collectDailyCommunitiesSubscribersStats(today, 'subscribers_change')
    } catch (e) {
      this.logger.error('Error collecting communities_subscribers_stats', e)
    }

    try {
      await this.collectDailyCommunitiesCommentStats(today, 'posts')
    } catch (e) {
      this.logger.error('Error collecting communities_post_stats', e)
    }

    try {
      await this.collectDailyCommunitiesCommentStats(today, 'comments')
    } catch (e) {
      this.logger.error('Error collecting communities_comment_stats', e)
    }

    try {
      await this.collectDailyCommunitiesUniqueAuthorStats(today, 'unique_post_authors')
    } catch (e) {
      this.logger.error('Error collecting communities_unique_post_authors', e)
    }

    try {
      await this.collectDailyCommunitiesUniqueAuthorStats(today, 'unique_comment_authors')
    } catch (e) {
      this.logger.error('Error collecting communities_unique_comment_authors', e)
    }

    try {
      await this.collectDailyCommunitiesPayoutStats('post_payouts_hbd_equiv')
    } catch (e) {
      this.logger.error('Error collecting communities_post_payouts_hbd_equiv', e)
    }

    try {
      await this.collectDailyCommunitiesPayoutStats('comment_payouts_hbd_equiv')
    } catch (e) {
      this.logger.error('Error collecting communities_comment_payouts_hbd_equiv', e)
    }

  }

  /*
   * Collect daily communities subscribers stats
   */
  async collectDailyCommunitiesSubscribersStats(today: string, metricName: string) {
    
    //Load the daily stats we already have
    var [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_communities_daily
      WHERE metric_name = ?
      ORDER BY day DESC
      LIMIT 1;`,
      [metricName]
    )
    
    //Collect since last collected date, otherwise since beginning of blockchain 
    let lastDate = (localStats.length > 0) ? localStats[0].day + " 23:59:59" : "1970-01-01 00:00:00"
    
    const communities = await this.hiveSqlService.executeQuery(
    `SELECT YEAR(timestamp) AS year, MONTH(timestamp) AS month, DAY(timestamp) AS day, JSON_VALUE(json,'$[1].community') AS community,
    COUNT(
      CASE
        WHEN JSON_VALUE(json,'$[0]') = 'subscribe' THEN 1 ELSE NULL
      END) AS subscribers,
    COUNT(
      CASE
        WHEN JSON_VALUE(json,'$[0]') = 'unsubscribe' THEN 1 ELSE NULL
      END) AS unsubscribers
    FROM TxCustoms
    WHERE tid = 'community'
    AND ISJSON(json) > 0
    AND TRIM(JSON_VALUE(json,'$[1].community')) LIKE 'hive-%'
    AND JSON_VALUE(json,'$[0]') IN('subscribe','unsubscribe')
    AND timestamp > :lastDateStr
    AND timestamp < :todayStr
    GROUP BY YEAR(timestamp), MONTH(timestamp), DAY(timestamp), JSON_VALUE(json,'$[1].community')
    ORDER BY year, month, day ASC;`,
    {lastDateStr: lastDate, todayStr:today}
    )
    this.logger.log(`Fetched ${communities.length} records`)

    const tr = await this.sequelize.transaction();
    
    try {
      for (const community of communities) {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_communities_daily (day, community, metric_name, value)
        VALUES (make_date(?, ?, ?), ?, ?, ?);`,
        [community.year, community.month, community.day, community.community, metricName, (community.subscribers - community.unsubscribers)],
        tr)
      }
      await tr.commit()
    } catch (e) {
      this.logger.error('Error:', e)
      await tr.rollback()
    }
    
    return
  }

  /*
   * Collect daily communities post and comment stats
   */
  async collectDailyCommunitiesCommentStats(today: string, metricName: string) {
    
    //Load the daily stats we already have
    var [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_communities_daily
      WHERE metric_name = ?
      ORDER BY day DESC
      LIMIT 1;`,
      [metricName]
    )
    
    //Collect since last collected date, otherwise since beginning of blockchain 
    let lastDate = (localStats.length > 0) ? localStats[0].day + " 23:59:59" : "1970-01-01 00:00:00"
    
    let operator
    if(metricName == 'posts') operator = '='
    if(metricName == 'comments') operator = '<>'

    const communities = await this.hiveSqlService.executeQuery(
    `SELECT YEAR(created) AS year, MONTH(created) AS month, DAY(created) AS day, category, COUNT(*) AS count
    FROM Comments
    WHERE category LIKE 'hive-[0-9][0-9][0-9][0-9][0-9]%'
    AND depth ${operator} 0
    AND created > :lastDateStr
    AND created < :todayStr
    GROUP BY YEAR(created), MONTH(created), DAY(created), category
    ORDER BY year, month, day, count DESC;`,
    {lastDateStr: lastDate, todayStr:today}
    )
    this.logger.log(`Fetched ${communities.length} records`)

    const tr = await this.sequelize.transaction();
    
    try {
      for (const community of communities) {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_communities_daily (day, community, metric_name, value)
        VALUES (make_date(?, ?, ?), ?, ?, ?);`,
        [community.year, community.month, community.day, community.category, metricName, community.count],
        tr)
      }
      await tr.commit()
    } catch (e) {
      this.logger.error('Error:', e)
      await tr.rollback()
    }
    
    return
  }

  /*
   * Collect daily communities unique post and comment authors
   */
  async collectDailyCommunitiesUniqueAuthorStats(today: string, metricName: string) {
    
    //Load the daily stats we already have
    var [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_communities_daily
      WHERE metric_name = ?
      ORDER BY day DESC
      LIMIT 1;`,
      [metricName]
    )
    
    //Collect since last collected date, otherwise since beginning of blockchain 
    let lastDate = (localStats.length > 0) ? localStats[0].day + " 23:59:59" : "1970-01-01 00:00:00"
    
    let operator
    if(metricName == 'unique_post_authors') operator = '='
    if(metricName == 'unique_comment_authors') operator = '<>'

    const communities = await this.hiveSqlService.executeQuery(
    `SELECT YEAR(created) AS year, MONTH(created) AS month, DAY(created) AS day, category, COUNT(DISTINCT(author)) AS unique_authors
    FROM Comments
    WHERE category LIKE 'hive-[0-9][0-9][0-9][0-9][0-9]%'
    AND depth ${operator} 0
    AND created > :lastDateStr
    AND created < :todayStr
    GROUP BY YEAR(created), MONTH(created), DAY(created), category
    ORDER BY year, month, day, unique_authors DESC;`,
    {lastDateStr: lastDate, todayStr:today}
    )
    this.logger.log(`Fetched ${communities.length} records`)

    const tr = await this.sequelize.transaction();
    
    try {
      for (const community of communities) {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_communities_daily (day, community, metric_name, value)
        VALUES (make_date(?, ?, ?), ?, ?, ?);`,
        [community.year, community.month, community.day, community.category, metricName, community.unique_authors],
        tr)
      }
      await tr.commit()
    } catch (e) {
      this.logger.error('Error:', e)
      await tr.rollback()
    }
    
    return
  }

  /*
   * Collect daily communities post and comment payouts, measured in HBD
   */
  async collectDailyCommunitiesPayoutStats(metricName: string) {

    /* Payouts happen 7 days after the creation of a post/comment.
    To ensure we collect payout stats for full days, we go back 7 days
    and we track only the already completed payouts */

    let d = new Date()
    d.setDate(d.getUTCDate() - 7)
    let sevenDaysAgo = await this.collectorServiceFunctions.getDateString(d)
    
    //Load the daily stats we already have
    var [localStats, localStatsMeta] = await this.localConnectionService.executeQuery(
      `SELECT day
      FROM hive_communities_daily
      WHERE metric_name = ?
      ORDER BY day DESC
      LIMIT 1;`,
      [metricName]
    )
    
    //Collect since last collected date, otherwise since beginning of blockchain 
    let lastDate = (localStats.length > 0) ? localStats[0].day + " 23:59:59" : "1970-01-01 00:00:00"
    
    let operator
    if(metricName == 'post_payouts_hbd_equiv') operator = '='
    if(metricName == 'comment_payouts_hbd_equiv') operator = '<>'

    const communities = await this.hiveSqlService.executeQuery(
    `SELECT YEAR(last_payout) AS year, MONTH(last_payout) AS month, DAY(last_payout) AS day, category, SUM(total_payout_value) AS hbd_payout
    FROM Comments
    WHERE category LIKE 'hive-[0-9][0-9][0-9][0-9][0-9]%'
    AND depth ${operator} 0
    AND total_payout_value <> 0
    AND last_payout <> '1969-12-31 23:59:59'
    AND last_payout > :lastDateStr
    AND last_payout < :sevenDaysAgoStr
    GROUP BY YEAR(last_payout), MONTH(last_payout), DAY(last_payout), category
    ORDER BY year, month, day, hbd_payout DESC;`,
    {lastDateStr: lastDate, sevenDaysAgoStr: sevenDaysAgo}
    )
    this.logger.log(`Fetched ${communities.length} records`)

    const tr = await this.sequelize.transaction();
    
    try {
      for (const community of communities) {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_communities_daily (day, community, metric_name, value)
        VALUES (make_date(?, ?, ?), ?, ?, ?);`,
        [community.year, community.month, community.day, community.category, metricName, (community.hbd_payout * 1000)],
        tr)
      }
      await tr.commit()
    } catch (e) {
      this.logger.error('Error:', e)
      await tr.rollback()
    }
    
    return
  }

  /*
   * Collect daily communities metadata
   */
  async collectCommunitiesMeta() {
    
    /* The metadata for communities can always change, so we just delete and repopulate what we have */
    
    const communities = await this.hiveSqlService.executeQuery(
    `SELECT *
    FROM Communities;`,
    {}
    )
    this.logger.log(`Fetched ${communities.length} records`)

    const tr = await this.sequelize.transaction();
    
    try {
      await this.localConnectionService.executeTranQuery(
        `DELETE FROM hive_communities_meta;`,
        [],
        tr
      )
      
      for (const community of communities) {
        await this.localConnectionService.executeTranQuery(
        `INSERT INTO hive_communities_meta (community_id, type, title, about, description, language, nsfw)
        VALUES (?, ?, ?, ?, ?, ?, ?);`,
        [community.name, community.type, community.title, community.about, community.description, community.language, community.nsfw],
        tr)
      }
      await tr.commit()
    } catch (e) {
      this.logger.error('Error:', e)
      await tr.rollback()
    }
    
    return
  }

}
