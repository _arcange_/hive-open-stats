import { Module } from '@nestjs/common'
import { SequelizeModule } from '@nestjs/sequelize'
import { HiveSqlModule } from '../hivesql/hiveSql.module'
import { LocalConnectionModule } from '../localconnection/localConnection.module'
import { HiveBeaconModule } from '../hivebeacon/hiveBeacon.module'
import { CollectorService } from './collector.service'
import { CollectorServiceFunctions } from './collector.service.functions'
import { CollectorServiceTransfers } from './collector.service.transfers'
import { CollectorServiceCommunities } from './collector.service.communities'

@Module({
  imports: [HiveSqlModule, LocalConnectionModule, HiveBeaconModule],
  controllers: [],
  providers: [CollectorService, CollectorServiceFunctions, CollectorServiceTransfers, CollectorServiceCommunities]
})
export class CollectorModule {}
