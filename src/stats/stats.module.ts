import { Module } from '@nestjs/common'
import { StatsController } from './stats.controller'
import { StatsService } from './stats.service'
import { LocalConnectionService } from '../localconnection/localConnection.service'
import { SettingsModule } from '../settings/settings.module'

@Module({
  imports: [],
  controllers: [StatsController],
  providers: [StatsService, LocalConnectionService]
})
export class StatsModule {}
