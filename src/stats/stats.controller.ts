import { Controller, Get, Param, Query } from '@nestjs/common'
import { ApiOperation } from '@nestjs/swagger'
import { StatsService } from './stats.service'

@Controller()
export class StatsController {
  constructor(private readonly statsService: StatsService) {}
  
  @Get('exchanges')
  @ApiOperation({description: `Accounts belonging to exchanges that have listed HIVE or Hive-Backed Dollars (HBD). This includes both centralized and decentralized exchanges.`})
  async exchanges(): Promise <string[]> {
    return await this.statsService.exchanges()
  }
  
  @Get('metrics')
  @ApiOperation({description: `Text strings with user-friendly descriptions of the statistics, suitable for use by frontends.`})
  async metrics(): Promise <any> {
    return await this.statsService.metrics()
  }

  @Get('tracked_apps')
  @ApiOperation({description: `A list of apps we track, along with their associated custom_json operations.`})
  async trackedApps(): Promise <any> {
    return await this.statsService.trackedApps()
  }

  @Get('posts/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime number of posts.`})
  async getPosts(@Param() params, @Query() query): Promise<number[]> {
    return await this.statsService.getComments('posts', params.time_unit, query)
  }
  
  @Get('comments/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime number of comments.`})
  async getComments(@Param() params, @Query() query): Promise<number[]> {
    return await this.statsService.getComments('comments', params.time_unit, query)
  }
  
  @Get('transfers_hive/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime number and total amount of transfers of HIVE. This includes all apps. It excludes HIVE sent to or from the Decentralized Hive Fund. It also excludes HIVE sent to or from exchanges. A full list of exchanges can be retrieved using the /exchanges endpoint.`})
  async transfersHive(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getTransfers('transfers_hive', params.time_unit, query)
  }
  
  @Get('transfers_hbd/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime number and total amount of transfers of HBD. This includes all apps. It excludes HBD sent to or from the Decentralized Hive Fund. It also excludes HBD sent to or from exchanges. A full list of exchanges can be retrieved using the /exchanges endpoint.`})
  async transfersHbd(@Param() params, @Query() query): Promise<number[]> {
    return await this.statsService.getTransfers('transfers_hbd', params.time_unit, query)
  }
  
  @Get('transfers_hive_from_exchanges/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime number and total amount of transfers of HIVE from exchanges to other accounts. This excludes transfers between exchanges. A full list of exchanges can be retrieved using the /exchanges endpoint.`})
  async transfersHiveFromExch(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getTransfers('transfers_hive_from_exchanges', params.time_unit, query)
  }
  
  @Get('transfers_hive_to_exchanges/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime number and total amount of transfers of HIVE to exchanges. This excludes transfers between exchanges. A full list of exchanges can be retrieved using the /exchanges endpoint.`})
  async transfersHiveToExch(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getTransfers('transfers_hive_to_exchanges', params.time_unit, query)
  }
  
  @Get('transfers_hbd_from_exchanges/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime number and total amount of transfers of HBD from exchanges to other accounts. This excludes transfers between exchanges. A full list of exchanges can be retrieved using the /exchanges endpoint.`})
  async transfersHbdFromExch(@Param() params, @Query() query): Promise<number[]> {
    return await this.statsService.getTransfers('transfers_hbd_from_exchanges', params.time_unit, query)
  }
  
  @Get('transfers_hbd_to_exchanges/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime number and total amount of transfers of HBD to exchanges. This excludes transfers between exchanges. A full list of exchanges can be retrieved using the /exchanges endpoint.`})
  async transfersHbdToExch(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getTransfers('transfers_hbd_to_exchanges', params.time_unit, query)
  }

  @Get('transfers_hive_to_null/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime number and total amount of transfers of HIVE to the @null account (i.e. burned).`})
  async transfersHiveToNull(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getTransfers('transfers_hive_to_null', params.time_unit, query)
  }

  @Get('transfers_hbd_to_null/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime number and total amount of transfers of HBD to the @null account (i.e. burned).`})
  async transfersHbdToNull(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getTransfers('transfers_hbd_to_null', params.time_unit, query)
  }
  
  @Get('power_ups/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime number and total amount of power ups (staking of HIVE). The amounts are in HIVE, not in VESTS.`})
  async powerUps(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getTransfers('power_ups', params.time_unit, query)
  }
  
  @Get('power_downs/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime number and total amount of power downs (unstaking of HIVE). This statistic represents the amount of power downs executed (amount of staked HIVE made liquid), not the initiated power downs. It excludes power downs which are immediately powered up on the same or a different account (the blockchain has a function to set a power down route and to optionally immediately power up the amount). The amounts are in HIVE, not in VESTS.`})
  async powerDowns(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getTransfers('power_downs', params.time_unit, query)
  }
  
  /*@Get('dhf_balance/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime `})
  async DhfBalance(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getDhfBalance(params.time_unit, query)
  }*/
  
  @Get('dhf_proposal_payments/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime number of funded proposals and the combined total amount of payments from the Decentralized Hive Fund (DHF) to those proposals. This excludes payments back to the DHF and to the @hbdstabilizer (that proposal is tracked in a separate metric). All payments are in HBD.`})
  async DhfProposalPayments(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getTransfers('DHF_proposal_payments', params.time_unit, query)
  }
  
  @Get('internal_market_volume/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime number of transactions and total volume of the internal market built into the Hive blockchain. Volume amounts are in HBD.`})
  async internalMarketVolume(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getTransfers('internal_market_volume', params.time_unit, query)
  }

  @Get('comment_rewards/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime number and total amount of rewards paid on posts and comments. This is the total rewards paid out, including author, curator and beneficiary rewards. The reward amount given is the Hive-Backed Dollars (HBD) equivalent of the payout, even if no HBD was actually paid out.`})
  async rewards(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getTransfers('total_comment_rewards_hbd_equiv', params.time_unit, query)
  }
  
  @Get('rc/:metric_name/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime Resource Credit cost of various operations. metric_name is cost_ followed by one of the Hive operations: https://gitlab.syncad.com/hive/hive/-/blob/master/libraries/protocol/include/hive/protocol/operations.hpp#L16 For time units greater than daily, the average value for the days within the time period is given.`})
  async getRC(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getRC(params.metric_name, params.time_unit, query)
  }

  @Get('operations/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime number of operations made on Hive, along with a breakdown by operation type.\n\n Note: Due to limitations, the 'custom' operation type, which is rarely used, is not included.`})
  async getOperations(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getOperations(params.time_unit, query)
  }

  @Get('operations/custom_json/:app/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime number of individual custom_json operations made. For a list of all the custom_json operations we track, see the /tracked_apps endpoint. \n\n Provide 'all_apps' for the {app} parameter to get the data for all the tracked apps.`})
  async getCustomJson(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getCustomJsons('custom_jsons', params.app, params.time_unit, query)
  }
  
  @Get('operations/apps/:app/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime number of operations a particular tracked app has made. For a list of tracked apps, see the /tracked_apps endpoint. \n\n Provide 'all_apps' for the {app} parameter to get the data for all the tracked apps.`})
  async getOperationsApp(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getCustomJsons('apps', params.app, params.time_unit, query)
  }

  @Get('total_hive_supply/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime HIVE in existence. For time units greater than daily, the average value for the days within the time period is given. \n\n Note: All amount values are rounded.`})
  async totalHiveSupply(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getMarket('total_hive_supply', params.time_unit, query)
  }

  @Get('total_hbd_supply/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime HBD in existence. For time units greater than daily, the average value for the days within the time period is given. \n\n Note: All amount values are rounded.`})
  async totalHbdSupply(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getMarket('total_hbd_supply', params.time_unit, query)
  }

  @Get('total_virtual_supply/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime Virtual Hive in existence. Virtual Hive is an abstract unit that shows how much HIVE there would be if all Hive-Backed Dollars (HBD) were to be converted to HIVE at current prices. Thus, it shows the total HIVE + HBD in existence. For time units greater than daily, the average value for the days within the time period is given. \n\n Note: All amount values are rounded.`})
  async totalVirtualSupply(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getMarket('total_virtual_supply', params.time_unit, query)
  }

  @Get('hbd_in_savings/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime amount of HBD moved to Savings, the interest paid, the amount moved from Savings, the net change (transferred to plus interest paid minus transferred from Savings), and the cumulative total amount of HBD in Savings.`})
  async HbdInSavings(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getHbdInSavings(params.time_unit, query)
  }

  @Get('exchange_hive/:exchange/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime amount of HIVE moved to and from an exchange (transferred to minus transferred from the exchange), the net change, and the cumulative total amount of HIVE on the exchange. For a list of all the tracked exchanges, see the /exchanges endpoint. Provide 'all_exchanges' for the {exchange} parameter to get the data for all the exchanges.`})
  async exchangeBalanceHive(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getExchBalance(params.exchange, 'HIVE', params.time_unit, query)
  }
  
  @Get('exchange_hbd/:exchange/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime amount of HBD moved to and from an exchange (transferred to minus transferred from the exchange), the net change, and the cumulative total amount of HBD on the exchange. For a list of all the tracked exchanges, see the /exchanges endpoint. Provide 'all_exchanges' for the {exchange} parameter to get the data for all the exchanges.`})
  async exchangeBalanceHbd(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getExchBalance(params.exchange, 'HBD', params.time_unit, query)
  }

  @Get('communities')
  @ApiOperation({description: `Get the existing communities, their metadata and their total statistics. \n\n To get the data for only a specific community, add 'c=hive-xxxxxx' as a query parameter, where hive-xxxxxx is the unique community identifier.`})
  async communities(@Param() params, @Query() query): Promise <any> {
    return await this.statsService.getCommunitiesMeta(query)
  }

  @Get('communities_totals')
  @ApiOperation({description: `Get the number of communities and the combined total statistics for all communities across Hive.`})
  async communitiesTotals(@Param() params, @Query() query): Promise <any> {
    return await this.statsService.getCommunitiesTotals(query)
  }

  @Get(':community_id/subscribers_change/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime change in subscribers (accounts subscribed minus unsubscribed) for a community, and the cumulative total subscribers. community_id is the unique identifier for a community, starting with 'hive-'.`})
  async communitySubscribersChange(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getCommunity(params.community_id, 'subscribers_change', params.time_unit, query)
  }

  @Get(':community_id/posts/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime posts made in a community, and the cumulative total posts. community_id is the unique identifier for a community, starting with 'hive-'.`})
  async communityPosts(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getCommunity(params.community_id, 'posts', params.time_unit, query)
  }

  @Get(':community_id/comments/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime comments made in a community, and the cumulative total comments. community_id is the unique identifier for a community, starting with 'hive-'.`})
  async communityComments(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getCommunity(params.community_id, 'comments', params.time_unit, query)
  }

  @Get(':community_id/unique_post_authors/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime unique post authors of a community. For time units greater than daily, the average value for the days within the time period is given. community_id is the unique identifier for a community, starting with 'hive-'.`})
  async communityUniquePostAuthors(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getCommunityUniqueAuthors(params.community_id, 'unique_post_authors', params.time_unit, query)
  }

  @Get(':community_id/unique_comment_authors/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime unique comment authors of a community. For time units greater than daily, the average value for the days within the time period is given. community_id is the unique identifier for a community, starting with 'hive-'.`})
  async communityUniqueCommentAuthors(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getCommunityUniqueAuthors(params.community_id, 'unique_comment_authors', params.time_unit, query)
  }

  @Get(':community_id/payouts/:time_unit')
  @ApiOperation({description: `Get the daily/weekly/monthly/yearly/lifetime payouts of posts and comments in a community. Payouts are given as measured in HBD (even if the actual payout was not in HBD). community_id is the unique identifier for a community, starting with 'hive-'.`})
  async communityPayouts(@Param() params, @Query() query): Promise <number[]> {
    return await this.statsService.getCommunityPayouts(params.community_id, params.time_unit, query)
  }
  
}
