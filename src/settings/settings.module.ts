import { Module } from '@nestjs/common'
import { SettingsService } from './settings.service'
import { CollectorServiceFunctions } from '../stats/collector.service.functions'
import { StatsService } from '../stats/stats.service'

@Module({
  imports: [],
  controllers: [],
  providers: [SettingsService, CollectorServiceFunctions, StatsService],
  exports: []
})
export class SettingsModule {}
