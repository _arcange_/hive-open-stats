import { Injectable } from '@nestjs/common'

@Injectable()
export class SettingsService {

  /*
   * Accounts belonging to exchanges
   */
  public exchanges = {
    'binance'           : ['deepcrypto8','binance-hot'],
    'bitfinex'          : ['bitfinex'],
    'bithumb'           : ['bithumb.hot','bithumb','bithumbsend2','bithumbrecv2','bt20hivedkdnel'],
    'bittrex'           : ['bittrex'],
    'blocktrades'       : ['blocktrades'],
    'coinpayments.net'  : ['coinpayments.net'],
    'dunamu'            : ['hot.dunamu','user.dunamu'],
    'gateio'            : ['gateiodeposit'],
    'gsr-io'            : ['gsr-io'],
    'hiveswap'          : ['hiveswap'], //Hive Engine
    'honey-swap'        : ['honey-swap'], //Hive Engine
    'huobi'             : ['huobi-pro','huobi-withdrawal'],
    'ionomy'            : ['ionomy'],
    'leodex'            : ['leodex'], //Hive Engine
    'mxchive'           : ['mxchive'],
    'openledger'        : ['openledger','openledger-dex'],
    'orinoco'           : ['orinoco'],
    'poloniex'          : ['poloniex'],
    'probit'            : ['probithive','probitred'],
    'rudex'             : ['rudex'],
    'upbit'             : ['upbitsteem','upbitsteemhot','upbit-exchange']
  }

  /*
   * Accounts belonging to exchanges - in a simple list
   */
  exchangeAccountsList(): string[] {
    
    let result = []

    for(var exchangeName in this.exchanges) {
      let assocHiveAccounts = this.exchanges[exchangeName]
      result = result.concat(assocHiveAccounts)
    }
    return result
  }

  /*
   * A list of operation types on the Hive blockchain
   */
   public operationTypes = ['account_create', 'account_create_with_delegation','account_update','account_update2','account_witness_proxy','account_witness_vote','cancel_transfer_from_savings','change_recovery_account','claim_account','claim_reward_balance','collateralized_convert','comment','comment_options','convert','create_claimed_account','create_proposal','custom_json','decline_voting_rights','delegate_vesting_shares','delete_comment','escrow_approve','escrow_dispute','escrow_release','escrow_transfer','feed_publish','limit_order_cancel','limit_order_create','limit_order_create2','pow','pow2','recover_account','recurrent_transfer','remove_proposal','request_account_recovery','set_withdraw_vesting_route','transfer','transfer_from_savings','transfer_to_savings','transfer_to_vesting','update_proposal','update_proposal_votes','vote','withdraw_vesting','witness_set_properties','witness_update']
   
   //Note: The above list doesn't include the 'custom' operation type. It is also missing from the derived HiveSQL tables. This operation is less utilized and can be found in the HiveSQL Transactions table.

   
   /*
    * List of apps which we track, and their associated custom_json operations
    */
   public trackedApps = [
     {'3speak'              : 'customJson3speak'},
     {'dcity'               : 'customJsonDcity'},
     {'dlux'                : 'customJsonDlux'},
     {'drugwars'            : 'customJsonDrugwars'},
     {'ecency'              : 'customJsonEcency'},
     {'exode'               : 'customJsonExode'},
     {'follow'              : 'customJsonFollow'}, //Hive core
     {'genesisleaguesports' : 'customJsonGenesisLeagueSports'},
     {'hive-engine'         : 'customJsonHiveEngine'},
     {'lensy'               : 'customJsonLensy'},
     {'nextcolony'          : 'customJsonNextColony'},
     {'nftmart'             : 'customJsonNftMart'},
     {'peakmonsters'        : 'customJsonPeakMonsters'},
     {'podping'             : 'customJsonPodping'},
     {'rabona'              : 'customJsonRabona'},
     {'ragnarok'            : 'customJsonRagnarok'},
     {'rc'                  : 'customJsonResourceCredits'}, //Hive core
     {'scotbot'             : 'customJsonScotbot'},
     {'splinterlands'       : 'customJsonSplinterlands'},
     {'spknetwork'          : 'customJsonSpkNetwork'},
     {'waivio'              : 'customJsonWaivio'}
   ]
   
   /*
    * Lists of custom_json operations that we track
    */
   public customJson3speak = [
     '3speak-live',
     '3speak-new',
     '3speak-topten',
     '3speak-trending-0',
     '3speak-trending-1',
     '3speak-trending-2',
     '3speak-trending-3',
     '3speak-trending-4',
     '3speak-update'
   ]
   
   public customJsonDcity = [
     'dcity',
     'dcity-anim-save',
     'dcity-bg-save',
     'dcity-save',
     'dcityelection',
     'dcitystats'
   ]
   
   public customJsonDlux = [
     'dlux_add_node',
     'dlux_cjv',
     'dlux_claim',
     'dlux_dex_buy',
     'dlux_dex_cancel',
     'dlux_dex_clear',
     'dlux_dex_hbd_sell',
     'dlux_dex_hive_sell',
     'dlux_dex_node_add',
     'dlux_dex_report',
     'dlux_dex_sell',
     'dlux_dex_send',
     'dlux_dex_steem_sell',
     'dlux_fts_sell_h',
     'dlux_fts_sell_hcancel',
     'dlux_ft_airdrop',
     'dlux_ft_auction',
     'dlux_ft_bid',
     'dlux_ft_buy',
     'dlux_ft_cancel_sell',
     'dlux_ft_escrow',
     'dlux_ft_escrow_cancel',
     'dlux_ft_escrow_complete',
     'dlux_ft_mint',
     'dlux_ft_sell',
     'dlux_ft_sell_cancel',
     'dlux_ft_transfer',
     'dlux_gov_down',
     'dlux_gov_up',
     'dlux_nft_add_roy',
     'dlux_nft_auction',
     'dlux_nft_bid',
     'dlux_nft_buy',
     'dlux_nft_define',
     'dlux_nft_delete',
     'dlux_nft_div',
     'dlux_nft_hauction',
     'dlux_nft_melt',
     'dlux_nft_mint',
     'dlux_nft_pfp',
     'dlux_nft_reserve_complete',
     'dlux_nft_reserve_transfer',
     'dlux_nft_reserve_transfer_cancel',
     'dlux_nft_sell',
     'dlux_nft_sell_cancel',
     'dlux_nft_transfer',
     'dlux_nft_transfer_cancel',
     'dlux_node_add',
     'dlux_nomention',
     'dlux_osig_submit',
     'dlux_power_down',
     'dlux_power_grant',
     'dlux_power_up',
     'dlux_report',
     'dlux_report_t',
     'dlux_send',
     'dlux_shares_claim',
     'dlux_sig_submit',
     'dlux_test_dex_steem_sell',
     'dlux_test_error_CF',
     'dlux_test_node_add',
     'dlux_test_node_delete',
     'dlux_test_power_down',
     'dlux_test_power_up',
     'dlux_test_report',
     'dlux_test_send',
     'dlux_token_dex_buy',
     'dlux_token_dex_cancel_buy',
     'dlux_token_dex_cancel_sell',
     'dlux_token_dex_sell',
     'dlux_token_node_add',
     'dlux_token_report',
     'dlux_token_send',
     'dlux_vote_content'
   ]
   
   public customJsonDrugwars = [
     'drugwars'
   ]
   
   public customJsonEcency = [
     'ecency_boost',
     'ecency_notify',
     'ecency_point_transfer',
     'ecency_promote',
     'ecency_registration'
   ]
   
   public customJsonExode = [
     'exode_2010_support_pack',
     'exode_accept_alpha2',
     'exode_bonuspacks',
     'exode_cancel_test_transaction',
     'exode_claim_debriefing_report',
     'exode_confirm_challenge',
     'exode_contract_dropready',
     'exode_delivery',
     'exode_dropdelivery',
     'exode_dygycon_6',
     'exode_dygycon_7',
     'exode_extinguish_flames',
     'exode_join_squadron',
     'exode_leave_alliance',
     'exode_leave_squadron',
     'exode_market_cancel_sell',
     'exode_market_sell',
     'exode_market_transfer',
     'exode_newgame',
     'exode_newpacks',
     'exode_openpack',
     'exode_reward_dygycon_swag',
     'exode_reward_medal',
     'exode_save_deck',
     'exode_start_game',
     'exode_upgrade_card',
     'exode_upgrade_confirmed'
   ]
   
   public customJsonFollow = [
     'follow'
   ]
   
   public customJsonGenesisLeagueSports = [
     'gls-plat',
     'gls-plat-create_applications',
     'gls-plat-create_assets',
     'gls-plat-create_collections',
     'gls-plat-mint_token',
     'gls-plat-price_feed',
     'gls-plat-stake_tokens',
     'gls-plat-token_transfer',
     'gls-plat-token_transfer_multi',
     'gls-plat-unstake_tokens'
   ]
   
   public customJsonHiveEngine = [
     'ssc-mainnet-hive'
   ]
   
   public customJsonLensy = [
     'lensy_apply_whitelist',
     'lensy_feature_photo',
     'lensy_manage_photo',
     'lensy_manage_user',
     'lensy_manual_issue',
     'lensy_process_application',
     'lensy_test_feature_photo',
     'lensy_test_update_profile',
     'lensy_update_profile'
   ]
   
   public customJsonNextColony = [
     'nextcolony'
   ]
   
   public customJsonNftMart = [
     'nftmart'
   ]
   
   public customJsonPeakMonsters = [
     'pm_accept_contract',
     'pm_auto_prices',
     'pm_break_contract',
     'pm_cancel_all_bids',
     'pm_cancel_all_rental_bids',
     'pm_cancel_bid',
     'pm_cancel_delegation',
     'pm_cancel_rental_bid',
     'pm_create_bid',
     'pm_credit_purchase',
     'pm_delegation_contract',
     'pm_market_purchase',
     'pm_new_delegation',
     'pm_pause_all_rental_bids',
     'pm_pause_auto_prices',
     'pm_pause_rental_bid',
     'pm_play_all_rental_bids',
     'pm_rental_bid',
     'pm_resume_all_rental_bids',
     'pm_stop_delegation',
     'pm_update_bid',
     'pm_update_settings'
   ]
   
   public customJsonPodping = [
     'podping',
     'podping-livetest',
     'podping-livetest_podcast_feed-up',
     'podping-startup'
   ]
   
   public customJsonRabona = [
     'rabona'
   ]
   
   public customJsonRagnarok = [
     'duat_claim',
     'duat_claim_drop',
     'duat_dex_clear',
     'duat_dex_sell',
     'duat_drop_claim',
     'duat_gov_down',
     'duat_gov_up',
     'duat_node_add',
     'duat_power_up',
     'duat_report',
     'duat_send',
     'duat_sig_submit'
   ]
   
   public customJsonResourceCredits = [
     'rc'
   ]
   
   public customJsonScotbot = [
     'scot-claim-token',
     'scotauto',
     'scotbot_op',
     'scot_claim',
     'scot_claim_token',
     'scot_create_claim',
     'scot_mute_post',
     'scot_payout_beneficiaries',
     'scot_set_tribe_settings',
     'scot_set_vote',
     'scot_set_vote',
     'scot_stake_token',
     'scot_unclaim_token'
   ]
   
   public customJsonSplinterlands = [
     'sl-hive',
     'sl_bridge-sps',
     'sl_sps_bridge',
     'sm_accept_challenge',
     'sm_account_oracle',
     'sm_add',
     'sm_add_wallet',
     'sm_advance_league',
     'sm_api_dump',
     'sm_approve_validator',
     'sm_assign_fray',
     'sm_ban_account',
     'sm_burn_cards',
     'sm_cancel_lottery_entry',
     'sm_cancel_match',
     'sm_cancel_rental',
     'sm_cancel_sell',
     'sm_cancel_tournament',
     'sm_cancel_unlock_assets',
     'sm_cancel_unstake_tokens',
     'sm_card_award',
     'sm_card_update',
     'sm_change_price',
     'sm_claim_airdrop',
     'sm_claim_quest',
     'sm_claim_raffle_prizes',
     'sm_claim_reward',
     'sm_claim_rewards',
     'sm_combine_all',
     'sm_combine_cards',
     'sm_convert_cards',
     'sm_create_guild',
     'sm_create_items',
     'sm_create_item_details',
     'sm_create_proposal',
     'sm_create_team',
     'sm_create_tournament',
     'sm_decline_challenge',
     'sm_delegate_cards',
     'sm_delegate_rewards',
     'sm_dev_cancel_undelegate_tokens',
     'sm_dev_delegate_tokens',
     'sm_dev_delegation_tokens',
     'sm_dev_undelegate_tokens',
     'sm_drop_league',
     'sm_edit_guild',
     'sm_enter_tournament',
     'sm_external_payment',
     'sm_find_match',
     'sm_gift-cards',
     'sm_gift_card',
     'sm_gift_cards',
     'sm_gift_match',
     'sm_gift_packs',
     'sm_gls_pack',
     'sm_gls_pack_transfer',
     'sm_grant_bonus_pack',
     'sm_guild_accept',
     'sm_guild_brawl_settings',
     'sm_guild_contribution',
     'sm_guild_decline',
     'sm_guild_decline_all',
     'sm_guild_demote',
     'sm_guild_disallow_cards',
     'sm_guild_invite',
     'sm_guild_promote',
     'sm_guild_purchase',
     'sm_guild_remove',
     'sm_join_guild',
     'sm_leave_fray',
     'sm_leave_guild',
     'sm_leave_tournament',
     'sm_lock_assets',
     'sm_market_buy',
     'sm_market_cancel_rent',
     'sm_market_cancel_rental',
     'sm_market_list',
     'sm_market_purchase',
     'sm_market_renew_rental',
     'sm_market_rent',
     'sm_open_all',
     'sm_open_pack',
     'sm_price_feed',
     'sm_proposal_vote',
     'sm_purchase',
     'sm_purchase_dice',
     'sm_purchase_item',
     'sm_purchase_land',
     'sm_purchase_land_lottery',
     'sm_purchase_orbs',
     'sm_purchase_record',
     'sm_purchase_skin',
     'sm_purchase_skin_set',
     'sm_ranked_settings',
     'sm_refresh_quest',
     'sm_refresh_team',
     'sm_refund_purchase',
     'sm_relinquish_spot',
     'sm_rent_cards',
     'sm_sell',
     'sm_sell_card',
     'sm_sell_cards',
     'sm_sell_purchase',
     'sm_set_authority',
     'sm_set_skin',
     'sm_set_title',
     'sm_shop_purchase',
     'sm_stake_tokens',
     'sm_start_match',
     'sm_start_quest',
     'sm_submit_team',
     'sm_surrender',
     'sm_team_reveal',
     'sm_token_award',
     'sm_token_transfer',
     'sm_token_transfer_multi',
     'sm_token_unstaking',
     'sm_tournament_checkin',
     'sm_tournament_payment',
     'sm_trade_title',
     'sm_transfer',
     'sm_transfer_items',
     'sm_transfer_skins',
     'sm_transfer_token',
     'sm_undelegate_cards',
     'sm_undelegate_rewards',
     'sm_unlock_assets',
     'sm_unstake_tokens',
     'sm_update_authority',
     'sm_update_price',
     'sm_update_rental_price',
     'sm_update_sell_price',
     'sm_update_validator',
     'sm_upgrade_account'
   ]
   
   public customJsonSpkNetwork = [
     'spk.bridge_id',
     'spkcc_claim',
     'spkcc_dex_clear',
     'spkcc_dex_sell',
     'spkcc_drop_claim',
     'spkcc_gov_down',
     'spkcc_gov_up',
     'spkcc_node_add',
     'spkcc_node_delete',
     'spkcc_nomention',
     'spkcc_osig_submit',
     'spkcc_power_down',
     'spkcc_power_grant',
     'spkcc_power_up',
     'spkcc_report',
     'spkcc_send',
     'spkcc_shares_claim',
     'spkcc_sig_submit',
     'spkcc_spk_send',
     'spkcc_val_add',
     'spkcc_val_del',
     'spk_claim',
     'spk_claimdrop',
     'spk_proposal',
     'spk_proposal_task',
     'spk_shares_claim'
   ]
   
   public customJsonWaivio = [
     'waivio_assign_campaign',
     'waivio_decline_campaign',
     'waivio_guest_account_update',
     'waivio_guest_bell',
     'waivio_guest_campaign',
     'waivio_guest_create',
     'waivio_guest_follow',
     'waivio_guest_follow_wobject',
     'waivio_guest_hide_comment',
     'waivio_guest_hide_post',
     'waivio_guest_notifications',
     'waivio_guest_reblog',
     'waivio_guest_vote',
     'waivio_guest_wobj_rating',
     'waivio_hive_engine',
     'waivio_sb_change_power',
     'waivio_sb_remove_rule',
     'waivio_sb_set_rule'
   ]
}

