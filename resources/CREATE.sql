CREATE TABLE IF NOT EXISTS hive_comments_daily (
    id SERIAL PRIMARY KEY,
    day DATE NOT NULL,
    type VARCHAR(255) NOT NULL,
    total_num BIGINT NOT NULL
);
CREATE TABLE IF NOT EXISTS hive_transfers_daily (
    id SERIAL PRIMARY KEY,
    day DATE NOT NULL,
    metric_name VARCHAR(255) NOT NULL,
    num BIGINT,
    amount BIGINT
);
CREATE TABLE IF NOT EXISTS hive_transfers (
    id SERIAL PRIMARY KEY,
    time_unit VARCHAR(255) NOT NULL,
    metric_name VARCHAR(255) NOT NULL,
    num BIGINT,
    amount BIGINT
);
CREATE TABLE IF NOT EXISTS hive_rc_daily (
    id SERIAL PRIMARY KEY,
    day DATE NOT NULL,
    metric_name VARCHAR(255) NOT NULL,
    value BIGINT NOT NULL
);
CREATE TABLE IF NOT EXISTS hive_operations_daily (
    id SERIAL PRIMARY KEY,
    day DATE NOT NULL,
    metric_name VARCHAR(255) NOT NULL,
    app VARCHAR(255),
    value BIGINT
);
CREATE TABLE IF NOT EXISTS hive_market_daily (
    id SERIAL PRIMARY KEY,
    day DATE NOT NULL,
    metric_name VARCHAR(255) NOT NULL,
    value BIGINT NOT NULL
);
CREATE TABLE IF NOT EXISTS hive_communities_daily (
    id SERIAL PRIMARY KEY,
    day DATE NOT NULL,
    community VARCHAR(255) NOT NULL,
    metric_name VARCHAR(255) NOT NULL,
    value BIGINT NOT NULL
);
CREATE TABLE IF NOT EXISTS hive_communities_meta (
    id SERIAL PRIMARY KEY,
    community_id VARCHAR(255) NOT NULL,
    type SMALLINT,
    title VARCHAR(255),
    about TEXT,
    description TEXT,
    language VARCHAR(255),
    nsfw BOOLEAN
);

/*** Populate text strings ***/
DROP TABLE IF EXISTS text_strings;
CREATE TABLE text_strings (
    id SERIAL PRIMARY KEY,
    key VARCHAR(255) UNIQUE NOT NULL,
    value VARCHAR NOT NULL
);
INSERT INTO text_strings (key, value)
VALUES
  ('posts', 'Number of posts made. This does not include comments.'),
  ('comments', 'Number of comments made.'),
  ('transfers_hive', 'Number and total amount of transfers of HIVE. Excludes HIVE sent to/from the Decentralized Hive Fund and exchanges.'),
  ('transfers_hbd', 'Number and total amount of transfers of Hive-Backed Dollars (HBD). Excludes HBD sent to/from the Decentralized Hive Fund and exchanges.'),
  ('transfers_hive_from_exchanges', 'Number and total amount of transfers of HIVE from exchanges to other accounts. Excludes transfers between exchanges.'),
  ('transfers_hive_to_exchanges', 'Number and total amount of transfers of HIVE to exchanges. Excludes transfers between exchanges.'),
  ('transfers_hbd_from_exchanges', 'Number and total amount of transfers of Hive-Backed Dollars (HBD) from exchanges to other accounts. Excludes transfers between exchanges.'),
  ('transfers_hbd_to_exchanges', 'Number and total amount of transfers of HBD to exchanges. Excludes transfers between exchanges.'),
  ('power_ups', 'Number and total amount of power ups (staking of HIVE).'),
  ('power_downs', 'Number and total amount of power downs (unstaking of HIVE). This shows the executed, not the initiated power downs. It excludes power downs which are immediately powered up (the blockchain has a function to set a power down route and to optionally immediately power up the amount).'),
  ('dhf_proposal_payments', 'Number of funded proposals and total HBD payments from the Decentralized Hive Fund (DHF) to those proposals. This excludes payments to the @hbdstabilizer.'),
  ('internal_market_volume', 'Number of transactions and total volume of the internal market built into the Hive blockchain. Volume amounts are in HBD.'),
  ('comment_rewards', 'Number and total amount of post and comment rewards. Includes author, curator and beneficiary rewards. The amounts are in the Hive-Backed Dollars (HBD) equivalent of the payout.'),
  ('rc', 'Resource Credit (RC) cost of various operations. HP Equivalent is the amount of Hive Power (HP) corresponding to the RC cost. For periods greater than daily, the averages are given.'),
  ('operations_apps', 'Total operations made by app.'),
  ('operations_custom_json', 'Individual custom_json operations made by app.'),
  ('total_hive_supply', 'Total amount of HIVE in existence. For periods greater than daily, the average for the period is given.'),
  ('total_hbd_supply', 'Total amount of Hive-Backed Dollars (HBD) in existence. For periods greater than daily, the average for the period is given.'),
  ('total_virtual_supply', 'Total amount of Virtual Hive in existence. Virtual Hive is an abstract unit that shows how much HIVE there would be if all Hive-Backed Dollars (HBD) were to be converted to HIVE at current prices. Thus, it shows the total HIVE + HBD in existence. For periods greater than daily, the average for the period is given.'),
  ('hbd_in_savings', 'Hive-Backed Dollars (HBD) in Savings - the amounts going in and out, the interest paid, the net change (amount in + interest paid - amount out), and the total cumulative amount of HBD in Savings.'),
  ('exchange_hive', 'HIVE on exchanges - the amounts going in and out, the net change, and the total cumulative amount of HIVE on exchanges.'),
  ('exchange_hbd', 'Hive-Backed Dollars (HBD) on exchanges - the amounts going in and out, the net change, and the total cumulative amount of HBD on exchanges.'),
  ('community_subscribers_change', 'The number of new subscribers a community gained (or lost) and the cumulative total subscribers.')
;