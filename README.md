# Hive Open Stats

A stats collection platform for the Hive ecosystem

## Installation

```bash
$ pnpm install
```

## Running the app

```bash
# development
$ pnpm run start

# watch mode
$ pnpm run start:dev

# production mode
$ pnpm run start:prod
```

## Test

```bash
# unit tests
$ pnpm run test

# e2e tests
$ pnpm run test:e2e

# test coverage
$ pnpm run test:cov
```

## Docker Container

```bash
# Clone the repository.

# Copy the environment sample file as your environment file
$ cp .env.sample .env

# Edit the .env file with your favourite flavour of text editor. Add strong random SQL username and password, and your HiveSQL credentials.
$ nano .env

# Build the docker image. Name it so it's ready to be pushed to the PeakD Gitlab container registry.
$ docker build . -t registry.gitlab.com/peakd/hive-open-stats

# Run docker image by mounting the PostgreSQL DB in a directory on the host machine. Replace /host/directory/for/db with the directory where you want the db to be put on your host. For production, replace 127.0.0.1 with your server's IP. 
$ docker run --rm -itd -p 127.0.0.1:3000:3000 --name=hiveopenstats -v /host/directory/for/db:/dbdata registry.gitlab.com/peakd/hive-open-stats



```

## License

Nest is [MIT licensed](LICENSE).

